﻿using Debug;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Timers;

namespace MSDL
{
    public class MessageQueue
    {
        public MessageQueue()
        {
            SendQueue = new Queue<object>();
            ReceivedQueue = new Queue<string>();
            messageTimer = new Timer(5000);
        }


        Timer messageTimer;

        private Queue<object> SendQueue;
        private Queue<string> ReceivedQueue;

        private Connection udpConnection = new Connection();
        private ModuleManager moduleManager = new ModuleManager();
        private ComputerDetails computerDetails = new ComputerDetails();

        private AddMessageToSendQueueDelegate addToSendQueueDelegate;
        private AddMessageToReceiveQueueDelegate addToReceiveQueueDelegate;
        private CreateNewMessageDelegate createNewMessageDelegate;
        private SendDelegate sendMessageDelegate;

        public AddMessageToSendQueueDelegate getAddToSendQueueDelegate()
        {
            return addToSendQueueDelegate;
        }

        public AddMessageToReceiveQueueDelegate getAddToReceiveQueueDelegate()
        {
            return addToReceiveQueueDelegate;
        }

        public CreateNewMessageDelegate getCreateNewMessageDelegate()
        {
            return createNewMessageDelegate;
        }

        public void Initialize()
        {
            Log.l("Initializing MessageQueue");
            addToSendQueueDelegate = new AddMessageToSendQueueDelegate(SendQueue.Enqueue);
            addToReceiveQueueDelegate = new AddMessageToReceiveQueueDelegate(ReceivedQueue.Enqueue);
            createNewMessageDelegate = new CreateNewMessageDelegate(MessageConverter.CreateNewMessage);

            udpConnection.Initialize(addToReceiveQueueDelegate);
            moduleManager.Initialize(addToSendQueueDelegate, createNewMessageDelegate);
            computerDetails.Initialize(addToSendQueueDelegate, createNewMessageDelegate);

            sendMessageDelegate = udpConnection.getSendDelegate();
            messageTimer.Elapsed += DistributeMessages;
            messageTimer.Start();
        }

        private void DistributeMessages(object source, ElapsedEventArgs e)
        {
            Log.l("Distributing messages!");
            Log.l("Number in send queue: " + SendQueue.Count);
            Log.l("Number in receive queue: " + ReceivedQueue.Count);
            while (ReceivedQueue.Count != 0)
            {
                dynamic message = MessageConverter.StringToObject(ReceivedQueue.Dequeue());
                foreach (IModule module in moduleManager.getModules())
                {
                    Log.l(message.name);
                    module.ModuleEntry(message);
                }
            }

            while(SendQueue.Count != 0)
            {
                sendMessageDelegate.Invoke(MessageConverter.ObjectToString(SendQueue.Dequeue()));
            }
        }

        /// <summary>
        /// Message Convertions
        /// </summary>
        class MessageConverter
        {
            public static object StringToObject(string message)
            {
                return JObject.Parse(message);
            }

            public static string ObjectToString(object messageObject)
            {
                dynamic obj = messageObject;
                return obj.ToString();
            }

            public static dynamic CreateNewMessage()
            {
                return new JObject();
            }
        }
    }
}
