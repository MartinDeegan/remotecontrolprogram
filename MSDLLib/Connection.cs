﻿using System;
using System.Net;
using System.Text;
using System.Net.Sockets;
using Debug;

namespace MSDL
{
    public class Connection
    {
        IPEndPoint RemoteEndPoint;
        IPEndPoint LocalEndPoint;
        UdpClient client;

        public Connection()
        {
            RemoteEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8125);
            LocalEndPoint = new IPEndPoint(IPAddress.Any, 8125);
            client = new UdpClient();
        }

        private AddMessageToReceiveQueueDelegate addToReceiveQueueDelegate;

        public SendDelegate getSendDelegate()
        {
            return new SendDelegate(SendMessage);
        }

        public void Initialize(AddMessageToReceiveQueueDelegate addToReceiveQueueDelegate)
        {
            Log.l("Initializing UDPConnection...");
            client.Connect(RemoteEndPoint);
            Log.l("done.");
            this.addToReceiveQueueDelegate = addToReceiveQueueDelegate;
            Receive();
        }

        private void SendMessage(string message)
        {
            Send(Encoding.Unicode.GetBytes(message));
        }

        private void Send(byte [] data)
        {
            client.BeginSend(data, data.Length, new AsyncCallback(SendCallback), null);
        }

        private void SendCallback(IAsyncResult ar)
        {
            client.EndSend(ar);
        }

        private void Receive()
        {
            client.BeginReceive(new AsyncCallback(ReceiveCallback), null);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                byte[] receivedBytes = client.EndReceive(ar, ref LocalEndPoint);
                string decodedMessage = Encoding.ASCII.GetString(receivedBytes);
                addToReceiveQueueDelegate.Invoke(decodedMessage);
            }
            catch(Exception e)
            {
                Log.l(e.Message);
            }
            Receive();
        }
    }
}
