﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Debug;

namespace MSDL
{
    public class ModuleManager
    {
        public ModuleManager()
        {

        }

        private List<IModule> modules;

        public List<IModule> getModules()
        {
            return modules;
        }

        public void Initialize(AddMessageToSendQueueDelegate AddMessageToQueueDelegate, CreateNewMessageDelegate CreateNewMessageDelegate)
        {
            Log.l("Initializing ModuleManager");
            modules = new List<IModule>();
            string dir = AppDomain.CurrentDomain.BaseDirectory;
            string [] dlls = Directory.GetFiles(dir, "*.dll");
            foreach(string dllPath in dlls)
            {
                Assembly loadedDll = Assembly.LoadFrom(dllPath);
                foreach(Type type in loadedDll.GetExportedTypes())
                {
                    if (type.FullName.StartsWith("Module"))
                    {
                        try
                        {
                            IModule loadedClass = (IModule)Activator.CreateInstance(type);
                            modules.Add(loadedClass);
                            loadedClass.Initialize(AddMessageToQueueDelegate, CreateNewMessageDelegate);
                        }
                        catch(Exception e)
                        {
                            Log.l(e.Message);
                            continue;
                        }
                    }
                }
            }
        }
    }
}
