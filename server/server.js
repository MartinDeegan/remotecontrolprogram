"use strict";
const dgram = require('dgram');
const server = dgram.createSocket('udp4');

var dbmanager = require('databasemanager.js');
db = dbmanager('computer')

server.on('error', function(err){
	console.log('server error:\n${err.stack}');
});

server.on('message', function(msg, rinfo){
	let ip = rinfo.address;
	let port = rinfo.port;
	console.log(msg.toString());
	var returnMessage = {};
	returnMessage.name = "Hello World";
	returnMessage.num = 123;
	var sendStr = JSON.stringify(returnMessage);
	server.send(sendStr, 0, sendStr.length, port, ip, (error) => {
		if (error) console.log(error);
	});

});

server.on('listening', function(){
	console.log('server listening on ' + server.address().address +':' + server.address().port);
});

server.bind(8125, 'localhost');
