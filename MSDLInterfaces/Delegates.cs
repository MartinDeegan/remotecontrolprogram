﻿namespace MSDL
{
    public delegate void SendDelegate(string toSend);
    public delegate void AddMessageToReceiveQueueDelegate(string message);
    public delegate void AddMessageToSendQueueDelegate(object message);
    public delegate dynamic CreateNewMessageDelegate();
    
}
