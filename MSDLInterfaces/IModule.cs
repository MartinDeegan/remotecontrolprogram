﻿namespace MSDL
{
    public interface IModule
    {
        void Initialize(AddMessageToSendQueueDelegate AddMessageToQueueDelegate, CreateNewMessageDelegate CreateNewMessageDelegate);
        void ModuleEntry(dynamic message);
        void GetModuleInformation();
    }
}
