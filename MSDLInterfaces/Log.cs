﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debug
{
    public class Log
    {
        private static List<string> logQueue = new List<string>();
        public static void l(string message)
        {
            logQueue.Add(message);
            log(); 
        }

        private static void log()
        {
            while (logQueue.Count != 0)
            {
                string message = logQueue[0];
                logQueue.RemoveAt(0);
#if DEBUG
                Console.WriteLine(message);
#elif RELEASE
            
#endif
            }
        }
    }
}
