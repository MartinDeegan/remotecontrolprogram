﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Item item = new Item("Martin", 17);
            string strObj = @"
  {
    'Title': 'Json.NET is awesome!',
    'Author': {
      'Name': 'James Newton-King',
      'Twitter': '@JamesNK',
      'Picture': '/jamesnk.png'
    },
   'Date': '2013-01-23T19:30:00',
    'BodyHtml': '&lt;h3&gt;Title!&lt;/h3&gt;\r\n&lt;p&gt;Content!&lt;/p&gt;'
  }
";
            Console.WriteLine(strObj);
            dynamic dynObj = JsonConvert.DeserializeObject<dynamic>(strObj);
            Console.WriteLine(dynObj.Title);
            Console.WriteLine(dynObj.Date);
            Console.ReadKey();
        }
    }

    class Item
    {
        public Item(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public string Name;
        public int Age;
    }
}
