﻿using MSDL;
using Debug;

/// <summary>
/// The namespace "Module" is necessary for the module to be loaded.
/// </summary>
namespace Module
{
    namespace MSDLModule
    {
        public class ModuleMain : IModule
        {
            /// <summary>
            /// Public constructor for activation.
            /// </summary>
            public ModuleMain()
            {
                
            }

            
            AddMessageToSendQueueDelegate addMessageToSendQueue;
            CreateNewMessageDelegate createNewMessage;

            /// <summary>
            /// Runs after the module is loaded. This is the first method called.
            /// </summary>
            /// <param name="AddMessageToQueueDelegate">
            /// A delegate to add a message to the send queue
            /// </param>
            public void Initialize(AddMessageToSendQueueDelegate AddMessageToQueueDelegate, CreateNewMessageDelegate CreateNewMessageDelegate)
            {
                addMessageToSendQueue = AddMessageToQueueDelegate;
                createNewMessage = CreateNewMessageDelegate;
            }

            /// <summary>
            /// Entry point for all messages from the message queue.
            /// </summary>
            /// <param name="message">
            /// The message recieved from the message queue
            /// </param>
            public void ModuleEntry(dynamic message)
            {
                Log.l("Module Entry");
                Log.l((String)message.name);
                dynamic newMessage = createNewMessage.Invoke();
                newMessage.name = message.name;
                newMessage.num = message.num;
                addMessageToSendQueue.Invoke(newMessage);
            }


            /// <summary>
            /// Module Information.
            /// </summary>
            private const string NAME = "Template Module";

            public void GetModuleInformation()
            {

            }
        }
    }
}
