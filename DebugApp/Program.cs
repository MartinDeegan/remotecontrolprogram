﻿using System;
using MSDL;


namespace DebugApp
{
    class Program
    {
        public static void Main(string[] args)
        {
            MessageQueue messageQueue = new MessageQueue();
            ModuleManager moduleManager = new ModuleManager();
            ComputerDetails computerDetails = new ComputerDetails();
            Initialize(messageQueue, moduleManager, computerDetails);
            Console.ReadKey();
        }

        static void Initialize(MessageQueue messageQueue, ModuleManager moduleManager, ComputerDetails computerDetails)
        {
            Console.WriteLine("Starting Initialization");
            messageQueue.Initialize();

        }

    }
}
